import json
import couchdb

couchserver = couchdb.Server("http://localhost:8091/")  # Couchbase Server'ına bağlandı.

db = "travel-sample"  # Server'ımızda bulunan bir databaseye bağlandık.
if db in couchserver:
    database = couchserver[db]


for documents in database:  # Bulduğumuz databaseye ait dökümanları bir listeye ekledik.
    doc = []
    doc.append(documents)


with open('data.txt', 'w') as output:  # Aldığımız verilerin bir json dosyası olarak çıkarılıyor.
    json.dump(doc, output)
