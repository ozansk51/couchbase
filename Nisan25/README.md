Case 1:

1. Öncelikle couchbase server kurulumumuzu makinemize yapıyoruz:

```
curl -O https://packages.couchbase.com/releases/couchbase-release/couchbase-release-1.0-x86_64.rpm
sudo rpm -i ./couchbase-release-1.0-x86_64.rpm
sudo yum install couchbase-server

```

2. Daha sonra kurmuş olduğumuz server'ı başlatıyoruz:

`systemctl start couchbase-server`

3. Artık localhost'umuzda couchbase-server'ına sahibiz. Burada yeni bir cluster oluşturuyoruz. Gerekli data, query vs. boyutlarını giriyoruz. Bu aşamadan sonra gelen ekranda server node eklemeleri yapacağız. Bunun için couchbase'in bize sunmuş olduğu serverlar'ı kullanabiliriz. Docker üzerinden bu serverları ayağı kaldıracağız:

```
`docker run -d --name db1 couchbase`  // 2 adet farklı ip'lerde çalışan node oluştu.
`docker run -d --name db2 couchbase`

```

4. Couchbase-server üzerinde 'Add Server' ile birlikte bu container'ların ip'sine ulaşarak node ekliyoruz:
NOTE: Oluşturulan yeni node'lar için yapacağı ve ayarlayacağı servisleri belilrleyebiliyoruz(Data, Index, Query, Search, Analytics, Eventing)

`docker inspect --format '{{ .NetworkSettings.IPAddress }}' db1` // db1'e ait ip'ye ulaşabiliyoruz.

5. Edindiğimiz ip ile yeni node'ları açıyoruz. 

6. Oluşturduğumuz node'ları rebalance ediyoruz.

7. Bucket oluşturmak yerine 'Settings' kısmında 'Sample Buckets' eklemesi yapabiliriz. Sample Bucket üzerinden  
'travel-sample' örneğini indirelim.

8. Search kısmından indirdiğimiz bucket'a ait bir index oluşturabiliriz. Bunun için bir JSON dosyası oluşturulabilir.

9. Burada bir dağınık yapıda çalışan iki node elde etmiş oluyoruz. Verdiğimiz servislere göre oluşturulan node'lara çeşitli özellik sağlayabiliyoruz. Örneğin search ve index servisini bir node'a eklemezsek, bu node oluşturduğumuz search index'ine ulaşamayacaktı. Fakat bütün servislere ait node'lar oluşturduğumuz index'e ulaşbilir halde olacaktır.

Case 2:

1. Öncelikle couchbase server'ını nerede başlatmışsak onu aktarıyoruz:

```
couchserver = couchdb.Server("http://localhost:8091/")

```

2. Daha sonra hangi databaseye ulşmak istiyorsak, onun ismiyle beraber server'a arıyoruz:

```
db = "travel-sample" 
if db in couchserver:
    database = couchserver[db]

```

3. Ele almış oluğumuz databaseye ait dökümanları bir listeye aktarıyoruz:

```
for documents in database:
    doc = []
    doc.append(documents)

```

4. Aldığımız dökümanları json file olrak yazdırıyoruz ve kaydediyoruz:

```
with open('data.txt', 'w') as output:
    json.dump(doc, output)

```


